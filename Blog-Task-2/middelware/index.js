const helperResponse = require("../helpers/HelperResponse")

exports.redirectIfAuthenticated = (req,res,next) => {
    if(helperResponse.getSession(req,"user")){
        return res.redirect("/");
    }else{
        res.render("login")
    }
}

exports.checkLogin = async function (req,res,next) {
    if(helperResponse.getSession(req,"user")){
        next();
    }else{
        return res.redirect("/login");
    }
}
