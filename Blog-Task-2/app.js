class App {

  constructor() {
    this.path = require("path")
    require("dotenv").config({ path: this.path.join(__dirname, ".env") })
    this.createError = require("http-errors");
    this.express = require("express");
    this.cookieParser = require("cookie-parser");
    this.bodyParser = require("body-parser");
    this.urlencodedParser = this.bodyParser.urlencoded({ extended: false })  
    this.expressSession = require("express-session")

    this.indexRouter = require("./routes/index");
    this.helperResponse = require("./helpers/HelperResponse")
    this.database = require("./database/Database")
    this.config = require("./config/Config")
    
    this.userService = require("./services/UserService");
    this.cors = require( "cors" )
    this.corsOptions = {
      origin: "*",
      optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
    }

    this.app = this.express();

// view engine setup
    this.app.set("views", this.path.join(__dirname, "views"));
    this.app.set("view engine", "ejs");

    this.app.use(this.express.json());
    this.app.use(this.express.urlencoded({ extended: false }));
    this.app.use(this.cookieParser());
    this.app.use(this.express.static(this.path.join(__dirname, "public")));


    this.app.use(this.expressSession({
      secret: this.config.commonConfig.SECRET_KEY,
      cookie: {
        maxAge: 24 * 60 * 60 * 1000 // 24 hours
      },
      resave: true,
      saveUninitialized: true
    } ) )

    
    this.app.use("/", this.indexRouter);


// catch 404 and forward to error handler
    this.app.use(function(req, res, next) {
      const createError = require("http-errors")
      next(createError(404));
    });



// error handler
    this.app.use(function (err, req, res, next) {
      let error = {}
      error.code = err.status || 400
      error.error = err.message || ""
      res.locals.message = err.message || ""
      res.locals.error = req.app.get("env") === "development" ? err : {}
      res.status(err.status || 500)
      res.render("error", error)
    })

    this.http = require("http")
    this.port = this.normalizePort(process.env.PORT || "3000")

    this.app.set("port", this.port)
    this.server = this.http.createServer(this.app)

    this.server.listen(this.port)
    this.server.on("error", this.onError)
    this.server.on("listening", this.onListening)
  }

  normalizePort(val) {
    const port = parseInt(val, 10)
    if (isNaN(port))
      return val
    if (port >= 0)
      return port
    return false
  }

  onError(error) {
    if (error.syscall !== "listen") throw error
    const bind = typeof this.port === "string" ? "Pipe " + this.port : "Port " + this.port
    switch (error.code) {
      case "EACCES":
        console.error(bind + " requires elevated privileges")
        process.exit(1)
        break
      case "EADDRINUSE":
        console.error(bind + " is already in use")
        process.exit(1)
        break
      default:
        throw error
    }
  }

  onListening() {
    const addr = this.address()
    const debug = require("debug")("server")
    const bind = typeof addr === "string" ?
      "pipe " + addr :
      "port " + addr.port
    debug("Listening on " + bind)
  }
}

const app = new App()

module.exports = app.app
