function addblog(data,status){
    for (let index = 0; index < data['blogs'].length; index++) {
        var str =  `<div class="post-preview">
                <a href="/post/${data['blogs'][index]['_id']}">
                    <h2 class="post-title">${data['blogs'][index]['title']}</h2>
                    <h3 class="post-subtitle">${data['blogs'][index]['description']}</h3>
                    <p class="post-meta">
                        Posted by
                        ${data['user'][index][0]['name']}
                        on ${data['blogs'][index]['createdAt'].substring(0,10)}
                    </p>
                </a>`
            if(res == data['blogs'][index]['user_id']){
                str+= `<a class="btn btn-success text-uppercase editBlog" data-id="${data['blogs'][index]['_id']}" style="margin-right:10px;">Edit</a>`
                str+= `<a class="btn btn-danger text-uppercase deleteBlog" data-id="${data['blogs'][index]['_id']}" style="margin-left:10px;">Delete</a>`
            }
            str+=`</div>
            <!-- Divider-->
            <hr class="my-4" />`
            if(status == "more"){
                $(".upper").before(str)
            }else{
                $(".blogData").append(str)
            }
    }
    if(status != "more"){
        $(".blogData").append(`<div class="d-flex justify-content-end mb-4 upper"><a class="btn btn-primary text-uppercase addMoreBlogs">Older Posts →</a></div>`)
    }
}

addblog(data,"default")

$(document).on("click",".addMoreBlogs",function(){
    var i = $(".post-preview")
    apiData = {
        lastData: i.length
    }
    Api.post("api/addMoreBlogs",null,apiData,function(error,res){
        if(error){
            console.log(error);
        }else{
            data = res.postData;
            addblog(data,"more")
        }
    })
})