$(document).on('click',".editBlog",function(){
    var blog_id = $(this).attr("data-id")
    window.location.href = `/edit-post/${blog_id}`
})

$(document).on('click',".deleteBlog",function(){
    var blog_id = $(this).attr("data-id")
    window.location.href = `/delete-post/${blog_id}`
})