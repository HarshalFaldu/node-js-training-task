if(operation == "edit"){
    $("#blogTitle").val(data2.blog.title)
    $("#blogDesc").val(data2.blog.description)
    $("#blogContent").val(data2.blog.content)
}


$(document).on("click","#post",function(){
    apiData = {
        title: $("#blogTitle").val(),
        description: $("#blogDesc").val(),
        content: $("#blogContent").val()
    }
    Api.post('api/addPost',null,apiData,function(error,res){
        if(res.status){
            window.location.href = "/index"
        }else{
            alert("Addition fails")
        }
    })
})

$(document).on("click","#edit",function(){
    apiData = {
        id : data2.blog._id,
        title: $("#blogTitle").val(),
        description: $("#blogDesc").val(),
        content: $("#blogContent").val()
    }
    Api.post('api/editSelectedBlog',null,apiData,function(error,res){
        if(res.status){
            window.location.href = "/index"
        }else{
            alert("Update fails")
        }
    })
})