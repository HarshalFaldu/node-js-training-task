class IndexRoute {
  constructor() {
    this.router = require("express").Router();
    this.middleware = require("../middelware");
    this.authController = require("../controllers/AuthController");
    this.dashboardController = require("../controllers/DashboardController");
    this.postController = require("../controllers/postController");
    this.bodyParser = require("body-parser");
    this.urlencodedParser = this.bodyParser.urlencoded({ extended: false })  
    this.setRoutes();
  }
  setRoutes() {

    // POST  

    // Register user
    this.router.post(
      "/register_user",
      this.authController.register.bind(this.authController)
    );

    // Check Login
    this.router.post(
      "/login_user",
      this.authController.login_user.bind(this.authController)
    );
    
    // Add blogs in database
    this.router.post(
      "/api/addPost",
      this.postController.addPost.bind(this.postController)
    );
    
    // Get older blogs
    this.router.post(
      "/api/addMoreBlogs",
      this.dashboardController.addMore.bind(this.dashboardController)
    );
    
    // Update a blog
    this.router.post(
      "/api/editSelectedBlog",
      this.postController.editSelectedBlog.bind(this.postController)
    );
    
    // GET
    // Index Page
    this.router.get(
      "/",
      this.dashboardController.home.bind(this.dashboardController)
    );

    // Logout
    this.router.get(
      "/logout",
      this.middleware.checkLogin,
      this.authController.logout.bind(this.authController)
    );

    // Login
    this.router.get(
      "/login",
      this.middleware.redirectIfAuthenticated,
      this.authController.logout.bind(this.authController)
    );
    
    // Redirect to Add Post page
    this.router.get(
      "/add-post",
      this.middleware.checkLogin,
      this.dashboardController.addPost.bind(this.dashboardController)
    );
    
    // Edit a selected blog
    this.router.get(
      "/edit-post/:id",
      this.middleware.checkLogin,
      this.dashboardController.editPost.bind(this.dashboardController)
    );

    // Deleted a selected blog
    this.router.get(
      "/delete-post/:id",
      this.middleware.checkLogin,
      this.dashboardController.deletePost.bind(this.dashboardController)
    );
    
    // Redirect to about page
    this.router.get(
      "/about",
      this.middleware.checkLogin,
      this.dashboardController.about.bind(this.dashboardController)
    );

    // Redirect to contact page
    this.router.get(
      "/contact",
      this.middleware.checkLogin,
      this.dashboardController.contact.bind(this.dashboardController)
    );

    // Redirect to index page
    this.router.get(
      "/index",
      this.dashboardController.home.bind(this.dashboardController)
    );
    
    // Show selected Blog
    this.router.get(
      "/post/:id",
      this.middleware.checkLogin,
      this.dashboardController.showBlog.bind(this.dashboardController)
    );

    // Redirect to register page
    this.router.get("/register", function(req, res, next) {
      res.render("register");
    });

  }
}
const router = new IndexRoute();
module.exports = router.router;