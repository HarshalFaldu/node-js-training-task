const blogModel = require("../models/blog");
const userModel = require("../models/users");

class blogService{
        /**
   *
   * Insert new Blog
   *
   * @param  {object}   request
   * @param  {object}   data
   * @return {object}
   */
   async registerBlog(req,data){
        var blog =new blogModel({ user_id: req.session["user"], title: data.title,description:data.description,content:data.content})
        return blog.save()
        .catch((err) => {return err})
    }

    /*
    * Get all blogs 
    *
    * @param  {object}   response
    * @return {object}
    */

    async getBlogs(res,data){
        var allBlogs = await blogModel.find({}).sort({title:1}).skip(data).limit(2)
        var user = [];
        for (let index = 0; index < allBlogs.length; index++) {
            var a = await userModel.find({email:new RegExp("^"+allBlogs[index]["user_id"]+"$", "i")})
            user.push(a)
        }
        return {
            blogs: allBlogs,
            user: user
        }
    }

    async findBlog(id){
        var blog = await blogModel.findById(id)
        var user = await userModel.find({email: new RegExp("^"+blog["user_id"]+"$", "i")})
        return {
            blog: blog,
            user: user
        }
    }

    async updateBlog(req,res){
        var updatedBlog = await blogModel.findOneAndUpdate({_id : res.id},{$set : {title: res.title, description: res.description,content: res.content}})
        return updatedBlog;
    }

    async deleteBlog(req){
        var deleteBlog = await blogModel.findOneAndDelete({_id : req})
        return deleteBlog
    }

}

module.exports = new blogService();