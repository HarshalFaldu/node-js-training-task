class Database{
    constructor() {
        this.mongoose = require("mongoose")
        this.connect();
    }

    connect(){
      this.mongoose
        .connect(process.env.MONGO_CNNSTR, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        .then(() => {
            console.log("DB - CONNECTED");
        })
        .catch( (error) => {
            console.log("Error: ",error);
        })
    }
}

module.exports = new Database();