const BaseController = require("./BaseController")

class postController extends BaseController{
    constructor() {
        super();
        this.router = require("express").Router();
        this.helperResponse = require("../helpers/HelperResponse")
        this.dashboardController = require("./DashboardController")
        this.middleware = require("../middelware")
        this.blogService = require("../services/blogService")
        this.es6BindAll = require("es6bindall");
        this.es6BindAll(this, ["addPost","editSelectedBlog"]);
        
        this.postValidation = this.joi.object({
            title : this.joi.string().required(),
            description : this.joi.string().required(),
            content : this.joi.string().required()
        });
    }


    /**
   *
   * Blog added in database 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      Blog added in database
   */
    async addPost(req,res){
        var validation = this.postValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",    
            });
        } else {
            var blog = await this.blogService.registerBlog(
                req,
                req.body
            )
            if(blog.title){
                res.send({status:true})
            }else {
                res.send({status:false})
            }
        }
       
    }
    
    
    /**
   *
   * Update a blog 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      Update a blog
   */
    async editSelectedBlog(req,res){
        var blog = await this.blogService.updateBlog(
            req,
            req.body
        )
        if(blog.title){
            res.send({status:true});
        }else {
            res.send({status:false});
        }  
    }
}

module.exports = new postController();