const BaseController = require("./BaseController")
const config = require("../config/Config")

class DashboardController extends BaseController{
    constructor() {
        super();
        this.blogService = require("../services/blogService")
        this.es6BindAll = require("es6bindall")
        this.es6BindAll(this, ["home","about","contact","addPost","addMore","editPost","deletePost","showBlog"])
    }
        /**
     *
     * home 
     *
     * @param  {object}   request
     * @param  {object}   response
     * @return {view}      home
     */
    async home(req,res){
        var postData = await this.blogService.getBlogs(res,0) 
        if(req.session.user){
            var status = "logout"
        } else {
            var status = "login"
        }
        res.render("index",{
            user: req.session.user,
            status: status,
            postData : JSON.stringify(postData),
        })
    }

    /**
   *
   * Add more blogs 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      Add more blogs
   */
    async addMore(req,res){
        var postData = await this.blogService.getBlogs(res,req.body.lastData)        
        res.send({
            user: req.session.user,
            postData : postData,
        })
    }


     /**
   *
   * About 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      About
   */
    async about(req,res){
        res.render("about",{
            status: "logout",
        })
    }
     
    /**
    *
    * Show specific Blog 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      Show specific Blog
    */
    async showBlog(req,res){

        var data = await this.blogService.findBlog(
            req.params.id
        )
        res.render("post",{
            status: "logout",
            data: JSON.stringify(data)
        })
    }
    
    /**
    *
    * Add Blog 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      Add Blog
    */
    async addPost(req,res){
        res.render("add-post",
        {
            status: "logout",
            operation: "post",
            data : {}
        }
        )
    }
    
    /**
    *
    * Edit a Blog 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      Edit a Blog
    */
    async editPost(req,res){
        var data = await this.blogService.findBlog(
            req.params.id
        )
        if(data){
            res.render("add-post",
            {
                status: "logout",
                operation: "edit",
                button:"Save",
                data : JSON.stringify(data)
            });
        } else {
            console.log("error in fetching data");
        }
    }

    /**
    *
    * Delete a Blog 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      Delete a Blog
    */
    async deletePost(req,res){
        var data = await this.blogService.deleteBlog(
            req.params.id
        )
        if(data){
            res.redirect("/")
        } else {
            console.log("error in deleting blog");
        }
    }
    
    /**
    *
    * contact 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      contact
    */
    async contact(req,res){
        res.render("contact",{
            status: "logout",
        })
    }
}

module.exports = new DashboardController();