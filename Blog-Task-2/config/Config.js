class config{
    constructor(port,commonConfig,messages) {
        this.port = port
        this.commonConfig = commonConfig
        this.messages = messages
    }

}
const port = process.env.DB_NAME;
const commonConfig = {
    APP_NAME : "Blogs website",
    APP_DESCRIPTION : "Blogs website",
    SECRET_KEY : process.env.SECRET_KEY
}

const message = {};
const configa = new config(port,commonConfig,message);
module.exports = configa;