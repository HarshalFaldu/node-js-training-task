var express = require('express');
var router = express.Router();
const sgMail = require('@sendgrid/mail')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('mail', { title: 'Express' });
});

router.post("/api/sendEmail",function(req,res){
  sgMail.setApiKey(process.env.SENDGRID_API_KEY)
  const msg = {
    "to": req.body.email,
    "from" : "dev024.weboccult@gmail.com",
    subject: 'Twilio Trial using Node js',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  }
  sgMail.send(msg)
  .then(() => {
    res.send({status:true,message:"Mail Sent Successfully"})
  },
  error=>{
    res.send({status:false,message:"Error in sending Mail"})
  })
})


router.post("/api/sendMessage",function(req,res){
  const accountSid = process.env.ACCOUNT_SID; 
  const authToken = process.env.AUTHENTICATIONKEY; 
  const client = require('twilio')(accountSid, authToken); 
 
  client.messages 
      .create({ 
         body: req.body.msg,  
         messagingServiceSid: process.env.MESSAGINGSERVICESID,      
         to: '+918320831797' 
       }) 
      .then((message) => {
        res.send({status:true,message:message.status})
      }) 
      .done();
})


module.exports = router;
