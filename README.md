# Task 2 #

Here the task is to create a website in which we can Add/Delete/Update/Read the blogs of ourselves or others. To use this repository you have to follow the following steps.

### Step 1 ###

* Clone the repository at the any directory
* Download node js if not downloaded earlier

### Step 2 ###

* Open terminal and go to the directory Blogs-Task-2 in terminal
* First of all you have to install all the dependencied.
* For that you have to run following command. ```npm install```

### Step 3 ###

* Now to start the project you have to run ``` npm start``` or if this isn't work then you can directly run the command ``` node app.js ``` to start the project

### Step 4 ###

* After running, search ``` http://localhost:3000/ ``` in browser and you can do new registration